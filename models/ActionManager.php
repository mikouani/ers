<?php

require_once "../core/Model.php";

class ActionManager extends Model{
    function getAll(){
        $db = $this->dbConnect();
        $req = $db->query("SELECT * FROM Action");
        return $req->fetchAll(PDO::FETCH_ASSOC);
    }

    function getFromId($id){
        $db = $this->dbConnect();
        $req = $db->query("SELECT * FROM Action WHERE id = ".$id);
        return $req->fetchAll(PDO::FETCH_ASSOC);
    }

    function getLast3AndNext3(){
        $db = $this->dbConnect();
        $req = $db->query("SELECT * FROM Action WHERE date>NOW() ORDER BY date, debut DESC LIMIT 3");
        $ret = $req->fetchAll(PDO::FETCH_ASSOC);
        $count = count($ret);
        $req = $db->query("SELECT * FROM Action WHERE date<=NOW() ORDER BY date, debut DESC LIMIT 3");
        foreach($req->fetchAll(PDO::FETCH_ASSOC) as $k=>$d)
            $ret[$count+$k] = $d;
        return $ret;
    }

    function addAction($titre, $lieu, $date, $debut, $fin){
        $db = $this->dbConnect();
        $req = $db->prepare("INSERT INTO `Action` (`titre`, `lieu`, `date`, `debut`, `fin`) VALUES (?, ?, ?, ?, ?)");
        if($req->execute(array($titre, $lieu, $date, $debut, $fin)))
            return true;
        else{
            var_dump($db->errorInfo());
            return false;
        }
    }

    function editAction($id, $titre, $lieu, $date, $debut, $fin){
        $db = $this->dbConnect();
        $req = $db->prepare("UPDATE Action SET titre = ?, lieu = ?, date = ?, debut = ?, fin = ? WHERE id = '" . $id. "'");
        if($req->execute(array($titre, $lieu, $date, $debut, $fin)))
            return true;
        else{
            var_dump($db->errorInfo());
            return false;
        }
    }

    function deleteAction($id){
        $db = $this->dbConnect();
        return $db->query("DELETE FROM Action WHERE id = '" . $id. "'");
    }
}

?>