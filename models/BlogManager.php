<?php

require_once "../core/Model.php";

class BlogManager extends Model{
    function getById($id){
        $db=$this->dbConnect();
        $req = $db->query("SELECT CONCAT(u.prenom, ' ', u.nom) AS unom, c.nom AS cnom, titre, contenu, ressource, date FROM Post AS p, Categorie AS c, Utilisateur AS u WHERE c.id=p.cat AND p.utilisateur=u.login AND p.id=".$id);
        return $req->fetchAll(PDO::FETCH_ASSOC);
    }

    function getByIdCheckRecipe($id){
        $db=$this->dbConnect();
        if($db->query("SELECT COUNT(*) FROM Recette WHERE id=".$id)->fetchColumn()){
            $req = $db->query("SELECT CONCAT(u.prenom, ' ', u.nom) AS unom, c.nom AS cnom, titre, contenu, ressource, date, tempsPrep, tempsCuisson, nbPersonnes FROM Post AS p, Categorie AS c, Utilisateur AS u, Recette AS r WHERE r.id=p.id AND c.id=p.cat AND p.utilisateur=u.login AND p.id=".$id);
        } else{
            $req = $db->query("SELECT CONCAT(u.prenom, ' ', u.nom) AS unom, c.nom AS cnom, titre, contenu, ressource, date FROM Post AS p, Categorie AS c, Utilisateur AS u WHERE c.id=p.cat AND p.utilisateur=u.login AND p.id=".$id);
        }
        return $req->fetchAll(PDO::FETCH_ASSOC);
    }

    function get10($page){
        $db=$this->dbConnect();
        $req = $db->query("SELECT p.id, c.nom AS cnom, CONCAT(u.prenom, ' ', u.nom) AS unom, titre, contenu, date, ressource FROM Post AS p, Categorie AS c, Utilisateur AS u WHERE c.id=p.cat AND p.utilisateur=u.login ORDER BY p.date DESC LIMIT ".(($page-1)*10).",10");
        return $req->fetchAll(PDO::FETCH_ASSOC);
    }

    function get25From($cat, $page){
        $db=$this->dbConnect();
        $req = $db->query("SELECT p.id, c.nom AS cnom, CONCAT(u.prenom, ' ', u.nom) AS unom, titre, contenu, date, ressource FROM Post AS p, Categorie AS c, Utilisateur AS u WHERE c.id=p.cat AND p.utilisateur=u.login AND ressource=0 AND c.nom='".$cat."' ORDER BY p.date DESC LIMIT ".(($page-1)*25).",25");
        return $req->fetchAll(PDO::FETCH_ASSOC);
    }

    function getRessourcesFrom($cat){
        $db=$this->dbConnect();
        $req = $db->query("SELECT p.id, c.nom AS cnom, CONCAT(u.prenom, ' ', u.nom) AS unom, titre, contenu, date, ressource FROM Post AS p, Categorie AS c, Utilisateur AS u WHERE c.id=p.cat AND p.utilisateur=u.login AND ressource=1 AND c.nom='".$cat."' ORDER BY p.date DESC");
        return $req->fetchAll(PDO::FETCH_ASSOC);
    }

    function getCount(){
        $db=$this->dbConnect();
        $req = $db->query("SELECT COUNT(*) FROM Post");
        return $req->fetch();
    }

    function getCountFrom($cat){
        $db=$this->dbConnect();
        $req = $db->query("SELECT COUNT(*) FROM Post AS p, Categorie AS c WHERE c.id=p.cat AND c.nom='".$cat."'");
        return $req->fetch();
    }

    function addPost($cat, $titre, $contenu, $auteur, $ressource){
        $db = $this->dbConnect();
        $db->beginTransaction();
        $req = $db->prepare("INSERT INTO Post(cat, titre, contenu, utilisateur, ressource) VALUES (?, ?, ?, ?, ?)");

        if($req->execute(array($cat, $titre, $contenu, $auteur, $ressource))){
            // Add Pictures to folder
            $id = $db->lastInsertId();
            $target_dir = "posts_img/";
            if(!empty($_FILES['miniature']["tmp_name"])) $target_min = $target_dir . 'min_' . $id . '.' . strtolower(pathinfo($_FILES['miniature']['name'], PATHINFO_EXTENSION));
            if(!empty($_FILES['couv']["tmp_name"])) $target_couv = $target_dir . 'couv_' . $id . '.' . strtolower(pathinfo($_FILES['couv']['name'], PATHINFO_EXTENSION));
            if(!empty($_FILES['illu']["tmp_name"])) $target_illu = $target_dir . 'illu_' . $id . '.' . strtolower(pathinfo($_FILES['illu']['name'], PATHINFO_EXTENSION));
            if((!empty($_FILES['miniature']["tmp_name"]) && !move_uploaded_file($_FILES["miniature"]["tmp_name"], $target_min)) || (!empty($_FILES['couv']["tmp_name"]) && !move_uploaded_file($_FILES["couv"]["tmp_name"], $target_couv)) || (!empty($_FILES['illu']["tmp_name"]) && !move_uploaded_file($_FILES["illu"]["tmp_name"], $target_illu))){
                $db->rollback();
                return false;
            }
            $db->commit();
            return $id;
        } else{
            $db->rollback();
            return false;
        }
    }

    function addRecette($cat, $titre, $contenu, $auteur, $ressource, $pers, $cuiss, $prep){
        $db = $this->dbConnect();
        $id=$this->addPost($cat, $titre, $contenu, $auteur, $ressource);
        if(!$id)
            return false;
        
        $req = $db->prepare("INSERT INTO Recette(id, tempsPrep, tempsCuisson, nbPersonnes) VALUES (?, ?, ?, ?)");

        if($req->execute(array($id, $prep, $cuiss, $pers)))
            return true;
        else
            return false;
    }

    function editPost($id, $cat, $titre, $contenu, $auteur, $ressource){
        $db = $this->dbConnect();
        $db->beginTransaction();
        // Update post in database
        $req = $db->prepare("UPDATE Post SET cat = ?, titre = ?, contenu = ?, utilisateur = ?, ressource = ? WHERE id = " . $id);

        if($req->execute(array($cat, $titre, $contenu, $auteur, $ressource))){
            // Check and delete if pictures already exists for this post
            $img = glob("../".$GLOBALS['publicName']."/posts_img/min_" . $id . ".*");
            if(!empty($_FILES['miniature']["tmp_name"]) && $img)
                if(!unlink($img[0])){
                    $db->rollback();
                    return false;
                }
            $img = glob("../".$GLOBALS['publicName']."/posts_img/couv_" . $id . ".*");
            if(!empty($_FILES['couv']["tmp_name"]) && $img)
                if(!unlink($img[0])){
                    $db->rollback();
                    return false;
                }
            $img = glob("../".$GLOBALS['publicName']."/posts_img/illu_" . $id . ".*");
            if(!empty($_FILES['illu']["tmp_name"]) && $img)
                if(!unlink($img[0])){
                    $db->rollback();
                    return false;
                }

            // Update Pictures to folder
            $target_dir = "posts_img/";
            if(!empty($_FILES['miniature']["tmp_name"])) $target_min = $target_dir . 'min_' . $id . '.' . strtolower(pathinfo($_FILES['miniature']['name'], PATHINFO_EXTENSION));
            if(!empty($_FILES['couv']["tmp_name"])) $target_couv = $target_dir . 'couv_' . $id . '.' . strtolower(pathinfo($_FILES['couv']['name'], PATHINFO_EXTENSION));
            if(!empty($_FILES['illu']["tmp_name"])) $target_illu = $target_dir . 'illu_' . $id . '.' . strtolower(pathinfo($_FILES['illu']['name'], PATHINFO_EXTENSION));
            if((!empty($_FILES['miniature']["tmp_name"]) && !move_uploaded_file($_FILES["miniature"]["tmp_name"], $target_min)) || (!empty($_FILES['couv']["tmp_name"]) && !move_uploaded_file($_FILES["couv"]["tmp_name"], $target_couv)) || (!empty($_FILES['illu']["tmp_name"]) && !move_uploaded_file($_FILES["illu"]["tmp_name"], $target_illu))){
                $db->rollback();
                return false;
            }
            $db->commit();
            return true;
        } else{
            $db->rollback();
            return false;
        }
    }

    function editRecette($id, $cat, $titre, $contenu, $auteur, $ressource, $prep, $cuiss, $pers){
        $db = $this->dbConnect();
        if(!$this->editPost($id, $cat, $titre, $contenu, $auteur, $ressource))
            return false;

        $req = $db->prepare("UPDATE Recette SET tempsPrep = ?, tempsCuisson = ?, nbPersonnes = ? WHERE id = " . $id);
        if($req->execute(array($prep, $cuiss, $pers)))
            return true;
        else
            return false;
    }

    function deletePost($id){
        $db = $this->dbConnect();
        $db->beginTransaction();
        if($db->query("DELETE FROM Post WHERE id = " . $id)){
            $result = glob("posts_img/min_".$id.".*");
            if($result)
                if(!unlink($result[0])){
                    $db->rollback();
                    return false;
                }
            $result = glob("posts_img/couv_".$id.".*");
            if($result)
                if(!unlink($result[0])){
                    $db->rollback();
                    return false;
                }
            $result = glob("posts_img/illu_".$id.".*");
            if($result)
                if(!unlink($result[0])){
                    $db->rollback();
                    return false;
                }
            $db->commit();
            return true;
        } else{
            $db->rollback();
            return false;
        }
    }
}

?>