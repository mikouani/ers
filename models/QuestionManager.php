<?php

require_once "../core/Model.php";

class QuestionManager extends Model{
    function getAnswered($nb, $page){
        $db = $this->dbConnect();
        $req = $db->query("SELECT * FROM Question WHERE reponse IS NOT NULL ORDER BY date DESC LIMIT ".(($page-1)*$nb).",".$nb);
        return $req->fetchAll(PDO::FETCH_ASSOC);
    }

    function getUnanswered($nb, $page){
        $db = $this->dbConnect();
        $req = $db->query("SELECT * FROM Question WHERE reponse IS NULL ORDER BY date LIMIT ".(($page-1)*$nb).",".$nb);
        return $req->fetchAll(PDO::FETCH_ASSOC);        
    }

    function getFromId($id){
        $db = $this->dbConnect();
        $req = $db->prepare("SELECT * FROM Question WHERE id=?");
        $req->execute(array($id));
        return $req->fetchAll(PDO::FETCH_ASSOC);
    }

    function getCountAnswered(){
        $db=$this->dbConnect();
        $req = $db->query("SELECT COUNT(*) FROM Question WHERE reponse IS NOT NULL");
        return $req->fetch();
    }

    function getCountUnanswered(){
        $db=$this->dbConnect();
        $req = $db->query("SELECT COUNT(*) FROM Question WHERE reponse IS NULL");
        return $req->fetch();
    }

    function addQuestion($intitule){
        $db = $this->dbConnect();
        $req = $db->prepare("INSERT INTO Question(intitule) VALUES (?)");
        if($req->execute(array($intitule)))
            return true;
        else{
            var_dump($db->errorInfo());
            return false;
        }
    }

    function editQuestion($id, $intitule, $reponse){
        $db = $this->dbConnect();
        $req = $db->prepare("UPDATE Question SET intitule = ?, reponse = ? WHERE id = " . $id);
        if($req->execute(array($intitule, $reponse)))
            return true;
        else{
            var_dump($db->errorInfo());
            return false;
        }
    }

    function deleteQuestion($id){
        $db = $this->dbConnect();
        return $db->query("DELETE FROM Question WHERE id = " . $id);
    }
}

?>