<?php

require_once "../core/Model.php";

class CategoryManager extends Model{
    function getAll(){
        $db = $this->dbConnect();
        $req = $db->query("SELECT * FROM Categorie");
        return $req->fetchAll(PDO::FETCH_ASSOC);
    }
}

?>