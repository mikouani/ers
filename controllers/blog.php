<?php

    // Get model dependencies
    require_once "../models/BlogManager.php";
    $blog = new BlogManager();

    // Get category from URL
    $cat = substr($_SERVER['REQUEST_URI'], 0, strrpos($_SERVER['REQUEST_URI'], '/'));
    $cat = substr($cat, strrpos($cat, '/') + 1);
    // Require view
    if($cat=="sommeil-stress") $cat="Sommeil & Stress";
    if($cat=="autre-addictions") $cat="Autre Addictions";
    $ressources = $blog->getRessourcesFrom(ucfirst($cat));
    $posts = $blog->get25From(ucfirst($cat), $page);
    $count = $blog->getCountFrom($cat)[0];
    $max = ceil($count/10);
    if($count==0){
        echo "Pas encore d'articles";
        die();
    }
    if($page>$max){
        ?>
            <script type="text/javascript">
            window.location.href = '<?= $GLOBALS['basePath'] ?>';
            </script>
        <?php
    }
    require_once "../views/blog.php";
?>