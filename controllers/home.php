<?php

    require_once "../models/ActionManager.php";
    require_once "../models/BlogManager.php";
    $actions = new ActionManager();
    $posts = new BlogManager();

    $blog = $posts->get10(1);
    $blog = array_splice($blog, 0, 5);
    $data = $actions->getLast3AndNext3();

    require_once "../views/home.php";

?>