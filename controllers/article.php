<?php

    // Get model dependencies
    require_once "../models/BlogManager.php";
    $blog = new BlogManager();

    // Require view
    if($post = $blog->getByIdCheckRecipe($id)){
        $post = $post[0];
        if(isset($post["nbPersonnes"])) $recette = true; else $recette = false;
        require_once "../views/article.php";
    } else{
        ?>
            <script type="text/javascript">
            window.location.href = '<?= $GLOBALS['basePath'] ?>';
            </script>
        <?php
    }
?>