<?php

    require_once "../core/Controller.php";

    // Require model
    require_once "../models/QuestionManager.php";
    $manager = new QuestionManager();

    if(!empty($_POST['question'])){
        if($manager->addQuestion($_POST['question']))
            $ok=TRUE;
        else
            $ok=FALSE;
    }

    // Get view
    $questions = $manager->getAnswered(6, $page);
    $count = $manager->getCountAnswered()[0];
    $max = ceil($count/6);
    $questions = $manager->getAnswered(6, $page);

    require_once "../views/boiteaquestions.php";
?>