    <script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bs-custom-file-input/dist/bs-custom-file-input.min.js"></script>

    <script type="text/javascript">
    $(document).ready(function(){
      // Activate tooltip
      $('[data-toggle="tooltip"]').tooltip();

      // Delete modal dashboard
      $('a[href$="#modal"]').click(function(event){
        event.preventDefault();
        $('#deleteConfirm').modal('show');
        let href = $(this).attr("href");
        let url = href.substr(0, href.length-6);
        $('#modalConfirm').click(function(){
          location.href=url;
        })
      });

      // Hide recipe checkbox and inputs
      $(".dRecette").hide();
      
      $("#cuiss, #prep, #pers, #ecuiss, #eprep, #epers").parent().hide();
      $('#addPostModal').click(function(){
        if($("#categorie option:selected").text() == "Nutrition")
          $(".dRecette").show();
        else
          $(".dRecette").hide();
      });
      $("#ecategorie option").change(function(){
        if($("#ecategorie option:selected").text() == "Nutrition")
          $(".dRecette").show();
        else
          $(".dRecette").hide();
      });

      $(".dRecette #recette, .dRecette #erecette").change(function(){
        if($(this).prop("checked") == true)
          $("#cuiss, #prep, #pers, #ecuiss, #eprep, #epers").parent().show();
        else
          $("#cuiss, #prep, #pers, #ecuiss, #eprep, #epers").parent().hide();
      });

      // Loading CRUD
      var $loading = $('#loading').hide();
      $(document)
        .ajaxStart(function () {
          $loading.show();
        })
        .ajaxStop(function () {
          $loading.hide();
        })
        .ajaxComplete(function(){ // Hide previous alerts
          $('.table-wrapper').prevAll(".alert").not(":last").remove();
        });

      // Init Custom File Input
      bsCustomFileInput.init();

      // Adding blog posts ajax trigger
      $('#addPostBtn').click(function(){
        $.ajax({
          type: "POST",
          processData: false,
		      contentType: false,
          url: "<?= $GLOBALS['basePath'] ?>/admin/blog/add",
          data: new FormData($('form').get(0)),
          success: function(data){
            $('.col.text-center.align-self-center').prepend(data);
          }
        });
      });

      // Edit button adds id in form and pre-fills data
      $('a.edit').click(function(){
        let value=$(this).attr("id");
        $('<input>').attr({
          'type':'hidden',
          'name':'post',
          'value':value
        }).appendTo('#editPostModal form');
        $.ajax({
          type:"POST",
          url:"<?= $GLOBALS['basePath'] ?>/articleData/"+value,
          datatype: 'json',
          success : function(data, status){
            data = JSON.parse(data);
            $('#editPostModal form input[name=titre]').val(data.titre);
            $('#ecategorie option').filter(function(){ return this.text == data.cnom; }).attr('selected', true).trigger("change");
            if(data.ressource != '0') $('#eressource').prop('checked', true);
            if(data.ressource == '0') $('#eressource').prop('checked', false);
            if(data.nbPersonnes){
              $('#erecette').prop('checked', true).trigger("change");
              $('#editPostModal form input[name=prep]').val(data.tempsPrep);
              $('#editPostModal form input[name=cuiss]').val(data.tempsCuisson);
              $('#editPostModal form input[name=pers]').val(data.nbPersonnes);
            }
            if(!data.nbPersonnes) $('#erecette').prop('checked', false).trigger("change");
            $('#eauteur option').filter(function(){ return this.text == data.unom; }).attr('selected', true);
            $('#editPostModal form textarea').val(data.contenu);
          }
        });
      });

      // Editing blog posts ajax trigger
      $('#editPostBtn').click(function(){
        $.ajax({
          type: "POST",
          processData: false,
		      contentType: false,
          url: "<?= $GLOBALS['basePath'] ?>/admin/blog/edit/"+$('#editPostModal form input[type=hidden]').val(),
          data: new FormData($('form').get(1)),
          success: function(data){
            $('.col.text-center.align-self-center').prepend(data);
          }
        });
      });

      // Delete button adds id in form
      $('a.delete').click(function(){
        let id = $(this).attr("id")
        // Deleting blog posts ajax trigger
        $('#delPostBtn').click(function(){
          $.ajax({
            type: "POST",
            url: "<?= $GLOBALS['basePath'] ?>/admin/blog/delete/"+id,
            success: function(data){
              $('.col.text-center.align-self-center').prepend(data);
            }
          });
        });
      });

      // Adding users ajax trigger
      $('#addUserBtn').click(function(){
        $.ajax({
          type: "POST",
          processData: false,
		      contentType: false,
          url: "<?= $GLOBALS['basePath'] ?>/admin/utilisateurs/add",
          data: new FormData($('form').get(0)),
          success: function(data){
            $('.col.text-center.align-self-center').prepend(data);
          }
        });
      });

      // Edit button adds login in form (user) and pre-fills data
      $('a.uedit').click(function(){
        let value=$(this).attr("id");
        $('<input>').attr({
          'type':'hidden',
          'name':'user',
          'value':value
        }).appendTo('#editUserModal form');
        $.ajax({
          type:"POST",
          url:"<?= $GLOBALS['basePath'] ?>/userData/"+value,
          datatype: 'json',
          success : function(data, status){
            data = JSON.parse(data);
            $('#editUserModal form input[name=fname]').val(data.prenom);
            $('#editUserModal form input[name=lname]').val(data.nom);
            $('#editUserModal form input[name=email]').val(data.email);
          }
        });
      });

      // Editing users ajax trigger
      $('#editUserBtn').click(function(){
        $.ajax({
          type: "POST",
          processData: false,
		      contentType: false,
          url: "<?= $GLOBALS['basePath'] ?>/admin/utilisateurs/edit/"+$('#editUserModal form input[type=hidden]').val(),
          data: new FormData($('form').get(1)),
          success: function(data){
            $('.col.text-center.align-self-center').prepend(data);
          }
        });
      });

      // Delete button adds login in form
      $('a.udelete').click(function(){
        let id = $(this).attr("id");
        // Deleting users ajax trigger
        $('#delUserBtn').click(function(){
          $.ajax({
            type: "POST",
            url: "<?= $GLOBALS['basePath'] ?>/admin/utilisateurs/delete/"+id,
            success: function(data){
              $('.col.text-center.align-self-center').prepend(data);
            }
          });
        });
      });

      // Adding actions ajax trigger
      $('#addActionBtn').click(function(){
        $.ajax({
          type: "POST",
          processData: false,
		      contentType: false,
          url: "<?= $GLOBALS['basePath'] ?>/admin/actions/add",
          data: new FormData($('form').get(0)),
          success: function(data){
            $('.col.text-center.align-self-center').prepend(data);
          }
        });
      });

      // Edit button adds id in form (action) and pre-fills data
      $('a.aedit').click(function(){
        let id = $(this).attr("id")
        $('<input>').attr({
          'type':'hidden',
          'name':'user',
          'value':id
        }).appendTo('#editActionModal form');
        $.ajax({
          type:"POST",
          url:"<?= $GLOBALS['basePath'] ?>/actionData/"+id,
          datatype: 'json',
          success : function(data, status){
            data = JSON.parse(data);
            $('#editActionModal form input[name=titre]').val(data.titre);
            $('#editActionModal form input[name=lieu]').val(data.lieu);
            $('#editActionModal form input[name=date]').val(data.date);
            $('#editActionModal form input[name=debut]').val(data.debut);
            $('#editActionModal form input[name=fin]').val(data.fin);
          }
        });
      });

      // Editing actions ajax trigger
      $('#editActionBtn').click(function(){
        $.ajax({
          type: "POST",
          processData: false,
		      contentType: false,
          url: "<?= $GLOBALS['basePath'] ?>/admin/actions/edit/"+$('#editActionModal form input[type=hidden]').val(),
          data: new FormData($('form').get(1)),
          success: function(data){
            $('.col.text-center.align-self-center').prepend(data);
          }
        });
      });

      // Delete button adds id in form
      $('a.adelete').click(function(){
        let id = $(this).attr("id")
        // Deleting actions ajax trigger
        $('#delActionBtn').click(function(){
          $.ajax({
            type: "POST",
            url: "<?= $GLOBALS['basePath'] ?>/admin/actions/delete/"+id,
            success: function(data){
              $('.col.text-center.align-self-center').prepend(data);
            }
          });
        });
      });

      // Edit button adds id in form (question) and pre-fills data
      $('a.qaedit').click(function(){
        let value = $(this).attr("id")
        $('<input>').attr({
          'type':'hidden',
          'name':'id',
          'value':value
        }).appendTo('#editQuestionModal form');
        $.ajax({
          type:"POST",
          url:"<?= $GLOBALS['basePath'] ?>/questionData/"+value,
          datatype: 'json',
          success : function(data, status){
            data = JSON.parse(data);
            $('#editQuestionModal form input[name=intitule]').val(data.intitule);
            $('#editQuestionModal form textarea').val(data.reponse);
          }
        });
      });

      // Edit button adds id in form (question) and pre-fills data
      $('a.quedit').click(function(){
        let value = $(this).attr("id");
        $('<input>').attr({
          'type':'hidden',
          'name':'id',
          'value': value
        }).appendTo('#editQuestionModal form');
        $.ajax({
          type:"POST",
          url:"<?= $GLOBALS['basePath'] ?>/questionData/"+value,
          datatype: 'json',
          success : function(data, status){
            data = JSON.parse(data);
            $('#editQuestionModal form input[name=intitule]').val(data.intitule);
          }
        });
      });

      // Editing actions ajax trigger
      $('#editQuestionBtn').click(function(){
        $.ajax({
          type: "POST",
          processData: false,
		      contentType: false,
          url: "<?= $GLOBALS['basePath'] ?>/admin/questions/edit/"+$('#editQuestionModal form input[type=hidden]').val(),
          data: new FormData($('form').get(0)),
          success: function(data){
            $('.col.text-center.align-self-center').first().prepend(data);
          }
        });
      });

      // Delete button adds id in form
      $('a.qadelete').click(function(){
        let id = $(this).attr("id")
        // Deleting actions ajax trigger
        $('#delQuestionBtn').click(function(){
          $.ajax({
            type: "POST",
            url: "<?= $GLOBALS['basePath'] ?>/admin/questions/delete/"+id,
            success: function(data){
              $('.col.text-center.align-self-center').first().prepend(data);
            }
          });
        });
      });

      // Delete button adds id in form
      $('a.qudelete').click(function(){
        let id = $(this).attr("id")
        // Deleting actions ajax trigger
        $('#delQuestionBtn').click(function(){
          $.ajax({
            type: "POST",
            url: "<?= $GLOBALS['basePath'] ?>/admin/questions/delete/"+id,
            success: function(data){
              $('.col.text-center.align-self-center').first().prepend(data);
            }
          });
        });
      });

    });
    </script>
  </body>
</html>