            </div>
            <div class="row w-100 mx-auto bg-light px-3 py-1">
                <div class="col-lg my-auto text-center">
                    <img src="<?= $GLOBALS['basePath'] ?>/rsc/logoutc.png" height=50>
                    <img src="<?= $GLOBALS['basePath'] ?>/rsc/logoregion.png" height=100>
                </div>
                <div class="col-lg my-auto text-center">
                    <a href="https://www.instagram.com/etudiants.relais.sante/"><img src="<?= $GLOBALS['basePath'] ?>/rsc/insta.png" height=35></a>
                    <a href="https://www.facebook.com/ErsUtc/"><img src="<?= $GLOBALS['basePath'] ?>/rsc/f.png" height=35></a>
                    <p class="m-0 mt-2 p-0" style="font-size:0.75em">Site réalisé par Anis MIKOU - anis.mikou@etu.utc.fr</p>
                </div>
                <div class="col-lg my-auto text-center">
                    <a href="https://utc.fr">Site de l'UTC</a>
            </div>
        </div>
        <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    </body>
</html>