<!doctype html>
<html lang="fr">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <link rel="icon" type="image/png" href="<?= $GLOBALS['basePath'] ?>/rsc/logo.png" />
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
        <link rel="stylesheet" href="<?= $GLOBALS['basePath'] ?>/rsc/style.css">
        <meta name="google-site-verification" content="FDqOfFxD3yPrNySEHHK4zLuisC8n-7Hw_vLqfQ46mmk" />
        <title>ERS | <?= ucfirst($match['name']) ?></title>
    </head>
    <body>
    <div class="container">
    <div class="row pt-4 w-100">
        <div class="col my-auto">
            <a href="<?= $router->generate('accueil') ?>" class="float-left"><img src="<?= $GLOBALS['basePath'] ?>/rsc/logo.png" width=132 height=150></a>
        </div>
        <div class="col my-auto">
            <img src="<?= $GLOBALS['basePath'] ?>/rsc/logoutcsanstexte.png" height=100 class="float-right">
        </div>
    </div>
    <div class="row">
        <nav class="navbar navbar-expand-lg navbar-light w-100" style="background:#F7DB09">
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav mx-auto">
                    <li class="nav-item">
                        <a class="nav-link" href="<?= $router->generate('accueil') ?>">Accueil</a>
                    </li>
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Addictions</a>
                        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                            <a class="dropdown-item" href="<?= $router->generate('alcool', array('page'=>1)) ?>">Alcool</a>
                            <a class="dropdown-item" href="<?= $router->generate('tabac', array('page'=>1)) ?>">Tabac</a>
                            <a class="dropdown-item" href="<?= $router->generate('autre Addictions', array('page'=>1)) ?>">Autre</a>
                        </div>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="<?= $router->generate('sexualite', array('page'=>1)) ?>">Sexualité</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="<?= $router->generate('sommeil & Stress', array('page'=>1)) ?>">Sommeil/Stress</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="<?= $router->generate('nutrition', array('page'=>1)) ?>">Nutrition</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="<?= $router->generate('divers', array('page'=>1)) ?>">Divers</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="<?= $router->generate('boite à Questions', array('page'=>1)) ?>">Boite à questions</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="<?= $router->generate('contacts') ?>">Contacts</a>
                    </li>
                </ul>
            </div>
        </nav>
    </div>
    <div class="row bg-light text-justify mb-4" id="content">