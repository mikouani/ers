<?php use Michelf\Markdown; ?>
<div class="col p-4">
    <h1 class="my-4"><?= ucfirst($cat) ?></h1>
    <?php if($cat=="Sommeil & Stress") $cat[0]="s"; ?>
    <?php if($cat=="Autre Addictions") $cat[0]="a"; ?>
    <?php if(!empty($ressources)) echo "<h2>Ressources</h2>"; ?>
    <?php
        foreach($ressources as $r){
            ?>
                <div class="card mb-4">
                    <div class="row no-gutters">
                        <div class="col-md-3">
                            <?php
                                echo '<img class="card-img-top mx-auto w-100 d-block" src="' . $GLOBALS['basePath'];
                                $img = glob("./posts_img/min_" . $r['id'] . ".*");
                                if(empty($img))
                                    echo "/rsc/unknown.png";
                                else
                                    echo $img[0];
                                echo '" alt="Card image cap" style="max-width:300px; max-height:300px">';
                            ?>
                        </div>
                        <div class="col-md-9">
                            <div class="card-body">
                                <h2 class="card-title"><?= $r['titre'] ?></h2>
                                <div class="card-text">
                                    <?php
                                        if(strlen($r['contenu']) > 256 )
                                             echo Markdown::defaultTransform(substr($r['contenu'],0,strpos($r['contenu'],' ',256)) . " ...");
                                        else
                                             echo $r['contenu'];
                                    ?>                                
                                </div>
                                <a href="<?= $GLOBALS['router']->generate("Article", array('id'=>$r['id'])) ?>" class="btn btn-warning">En savoir plus &rarr;</a>
                            </div>
                        </div>
                    </div>
                    <div class="card-footer text-muted bg-white">
                        Posté le <?= strftime('%m %B %Y à %H:%M', strtotime($r['date'])); ?> par <?= $r['unom'] ?>
                    </div>
                </div>
            <?php
        }
    ?>

    <h2>Articles</h2>

    <?php
        foreach($posts as $r){
            ?>
                <div class="card mb-4">
                    <div class="row no-gutters">
                        <div class="col-md-3">
                            <?php
                                echo '<img class="card-img-top mx-auto w-100 d-block" src="' . $GLOBALS['basePath'];
                                $img = glob("./posts_img/min_" . $r['id'] . ".*");
                                if(empty($img))
                                    echo "/rsc/unknown.png";
                                else
                                    echo $img[0];
                                echo '" alt="Card image cap" style="max-width:300px; max-height:300px;">';
                            ?>
                        </div>
                        <div class="col-md-9">
                            <div class="card-body">
                                <h2 class="card-title"><?= $r['titre'] ?></h2>
                                <div class="card-text">
                                    <?php
                                        if(strlen($r['contenu']) > 256 )
                                             echo Markdown::defaultTransform(substr($r['contenu'],0,strpos($r['contenu'],' ',256)) . " ...");
                                        else
                                             echo $r['contenu'];
                                    ?>
                                </div>
                                <a href="<?= $GLOBALS['router']->generate("Article", array('id'=>$r['id'])) ?>" class="btn btn-warning">En savoir plus &rarr;</a>
                            </div>
                        </div>
                    </div>
                    <div class="card-footer text-muted bg-white">
                        Posté le <?= strftime('%m %B %Y à %H:%M', strtotime($r['date'])); ?> par <?= $r['unom'] ?>
                    </div>
                </div>
            <?php
        }
    ?>

    <!-- Pagination -->
    <div class="mx-auto">
        <ul class="pagination">
            <li class="page-item <?php if($page==1){ echo "disabled"; } ?>"><a href="<?= $GLOBALS['router']->generate($cat, array('page'=>$page-1)) ?>" class="page-link">Précédant</a></li>
            <?php
                for($i=$page-2;$i<=$page+2;$i++){
                    if($i>0 && $i<=$max){
                        echo '<li class="page-item';
                        if($i==$page) echo " active";
                        echo '"><a href="#" class="page-link">'.$i.'</a></li>';
                    }
                }
            ?>
            <li class="page-item <?php if($page==$max){ echo "disabled"; }?>"><a href="<?= $GLOBALS['router']->generate($cat, array('page'=>$page+1)) ?>" class="page-link">Suivant</a></li>
        </ul>
        <div class="hint-text">Affichage de <b>25</b> posts sur <b><?= $count ?></b></div>
    </div>
</div>
<style>
.pagination {
    float: right;
    margin: 0 0 5px;
}
.pagination li a {
    border: none;
    font-size: 13px;
    min-width: 30px;
    min-height: 30px;
    color: #999;
    margin: 0 2px;
    line-height: 30px;
    border-radius: 2px !important;
    text-align: center;
    padding: 0 6px;
}
.pagination li a:hover {
    color: #666;
}	
.pagination li.active a, .pagination li.active a.page-link {
    background: #03A9F4;
}
.pagination li.active a:hover {        
    background: #0397d6;
}
.pagination li.disabled i {
    color: #ccc;
}
.pagination li i {
    font-size: 16px;
    padding-top: 6px
}
.hint-text {
    float: left;
    margin-top: 10px;
    font-size: 13px;
}
</style>