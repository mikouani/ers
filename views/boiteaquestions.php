<div class="col p-4">
    <?php
        if(isset($ok) && $ok)
            putSuccess("Question ajoutée !");
        else if(isset($ok) && $ok===FALSE)
            putError("Erreur lors de l'ajout");
    ?>
  <div>
  <h5>Pose une question !</h5>
  <form method="post" class="text-right">
      <div class="form-group">
          <textarea class="form-control" name="question" rows="5"></textarea>
      </div>
      <button type="submit" class="btn btn-primary">Envoyer</button>
  </form>
  </div>
    <h1 class="text-center mb-3">Questions posées</h1>
    <div class="w-75 mx-auto">
    <?php
        foreach($questions as $q){
            ?>
            <div class="question rounded p-3 mb-4">
                <div class="w-75 mb-3 pl-3 py-1 rounded text-white" style="background:#087CF8">
                    <h3 class="m-0"><?= $q['intitule'] ?></h3>
                    <small class="m-0">Posée le <?= strftime('%d/%m/%Y', strtotime($q['date'])); ?></small>
                </div>
                <p class="m-0 rounded p-3 w-75 ml-auto mr-0" style="background:white"><?= $q['reponse'] ?></p>
            </div>
            <?php
        }
    ?>
      <!-- Pagination -->
      <div class="mx-auto mt-4">
          <ul class="pagination">
              <li class="page-item <?php if($page==1){ echo "disabled"; } ?>"><a href="<?= $GLOBALS['router']->generate("boite à Questions", array('page'=>$page-1)) ?>" class="page-link">Précédant</a></li>
              <?php
                  for($i=$page-2;$i<=$page+2;$i++){
                      if($i>0 && $i<=$max){
                          echo '<li class="page-item';
                          if($i==$page) echo " active";
                          echo '"><a href="#" class="page-link">'.$i.'</a></li>';
                      }
                  }
              ?>
              <li class="page-item <?php if($page==$max){ echo "disabled"; }?>"><a href="<?= $GLOBALS['router']->generate("boite à Questions", array('page'=>$page+1)) ?>" class="page-link">Suivant</a></li>
          </ul>
          <div class="hint-text">Affichage de <b>6</b> questions sur <b><?= $count ?></b></div>
      </div>
    </div>
</div>
<style>
.pagination {
    float: right;
    margin: 0 0 5px;
}
.pagination li a {
    border: none;
    font-size: 13px;
    min-width: 30px;
    min-height: 30px;
    color: #999;
    margin: 0 2px;
    line-height: 30px;
    border-radius: 2px !important;
    text-align: center;
    padding: 0 6px;
}
.pagination li a:hover {
    color: #666;
}	
.pagination li.active a, .pagination li.active a.page-link {
    background: #03A9F4;
}
.pagination li.active a:hover {        
    background: #0397d6;
}
.pagination li.disabled i {
    color: #ccc;
}
.pagination li i {
    font-size: 16px;
    padding-top: 6px
}
.hint-text {
    float: left;
    margin-top: 10px;
    font-size: 13px;
}
</style>