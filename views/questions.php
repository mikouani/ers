<div class="container">
    <div class="row">
        <div class="col text-center align-self-center p-4">
        <div class="table-wrapper">
            <div class="table-title">
                <div class="row">
                    <div class="col-sm-6">
						<h2>Gestion Questions avec Réponses</h2>
					</div>
                </div>
            </div>
            <table class="table table-striped table-hover">
                <thead>
                    <tr>
                        <th>Intitule</th>
                        <th>Actions</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                        foreach($answered as $a){
                            ?>
                            <tr>
                                <td><?= $a['intitule'] ?></td>
                                <td>
                                    <a href="#editQuestionModal" class="qaedit" id="<?= $a['id'] ?>" data-toggle="modal"><i class="material-icons" data-toggle="tooltip" title="Modifier">&#xE254;</i></a>
                                    <a href="#deleteConfirm" class="qadelete" id="<?= $a['id'] ?>" data-toggle="modal"><i class="material-icons" data-toggle="tooltip" title="Supprimer">&#xE872;</i></a>
                                </td>
                            </tr>
                            <?php
                        }
                    ?>
                </tbody>
            </table>
            <div class="clearfix">
                <div class="hint-text">Affichage de <b>10</b> questions sur <b><?= $countA ?></b></div>
                <ul class="pagination">
                    <li class="page-item <?php if($page==1){ echo "disabled"; } ?>"><a href="<?= $GLOBALS['router']->generate("back-questions", array('page'=>$page-1)) ?>" class="page-link">Précédant</a></li>
                    <?php
                        for($i=$page-2;$i<=$page+2;$i++){
                            if($i>0 && $i<=$maxA){
                                echo '<li class="page-item';
                                if($i==$page) echo " active";
                                echo '"><a href="#" class="page-link">'.$i.'</a></li>';
                            }
                        }
                    ?>
                    <li class="page-item <?php if($page==$maxA){ echo "disabled"; }?>"><a href="<?= $GLOBALS['router']->generate("back-questions", array('page'=>$page+1)) ?>" class="page-link">Suivant</a></li>
                </ul>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col text-center align-self-center p-4">
        <div class="table-wrapper">
            <div class="table-title">
                <div class="row">
                    <div class="col-sm-6">
						<h2>Gestion Questions sans Réponses</h2>
					</div>
                </div>
            </div>
            <table class="table table-striped table-hover">
                <thead>
                    <tr>
                        <th>Intitule</th>
                        <th>Actions</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                        foreach($unanswered as $a){
                            ?>
                            <tr>
                                <td><?= $a['intitule'] ?></td>
                                <td>
                                    <a href="#editQuestionModal" class="quedit" id="<?= $a['id'] ?>" data-toggle="modal"><i class="material-icons" data-toggle="tooltip" title="Modifier">&#xE254;</i></a>
                                    <a href="#deleteConfirm" class="qudelete" id="<?= $a['id'] ?>" data-toggle="modal"><i class="material-icons" data-toggle="tooltip" title="Supprimer">&#xE872;</i></a>
                                </td>
                            </tr>
                            <?php
                        }
                    ?>
                </tbody>
            </table>
            <div class="clearfix">
                <div class="hint-text">Affichage de <b>10</b> posts sur <b><?= $countU ?></b></div>
                <ul class="pagination">
                    <li class="page-item <?php if($page==1){ echo "disabled"; } ?>"><a href="<?= $GLOBALS['router']->generate("back-questions", array('page'=>$page-1)) ?>" class="page-link">Précédant</a></li>
                    <?php
                        for($i=$page-2;$i<=$page+2;$i++){
                            if($i>0 && $i<=$maxU){
                                echo '<li class="page-item';
                                if($i==$page) echo " active";
                                echo '"><a href="#" class="page-link">'.$i.'</a></li>';
                            }
                        }
                    ?>
                    <li class="page-item <?php if($page==$maxU){ echo "disabled"; }?>"><a href="<?= $GLOBALS['router']->generate("back-questions", array('page'=>$page+1)) ?>" class="page-link">Suivant</a></li>
                </ul>
            </div>
        </div>
    </div>
</div>
<!-- Edit Modal HTML -->
<div id="editQuestionModal" class="modal fade">
    <div class="modal-dialog modal-xl">
        <div class="modal-content">
            <form>
                <div class="modal-header">						
                    <h4 class="modal-title">Modifier la question</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                </div>
                <div class="modal-body">					
                    <div class="form-group">
                        <label for="eintitule">Intitule</label>
                        <input type="text" class="form-control" name="intitule" id="eintitule" required>
                    </div>
                    <div class="form-group">
                        <label for="erep">Réponse</label>
                        <textarea class="form-control" name="reponse" id="erep" rows=5 required></textarea>
                    </div>
                </div>
                <div class="modal-footer">
                    <input type="button" class="btn btn-default" data-dismiss="modal" value="Annuler">
                    <input type="button" class="btn btn-info" data-dismiss="modal" id="editQuestionBtn" value="Enregistrer">
                </div>
            </form>
        </div>
    </div>
</div>
<!-- Delete Modal HTML -->
<div id="deleteConfirm" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">						
                <h4 class="modal-title">Supprimer la question</h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            </div>
            <div class="modal-body">					
                <p>Etes-vous sur de vouloir supprimer cette question ?</p>
                <p class="text-danger"><small>C'est irréversible</small></p>
            </div>
            <div class="modal-footer">
                <input type="button" class="btn btn-default" data-dismiss="modal" value="Annuler">
                <input type="button" id="delQuestionBtn" data-dismiss="modal" class="btn btn-danger" value="Supprimer">
            </div>
        </div>
    </div>
</div>
<style>
.table-wrapper {
    background: #fff;
    margin: 30px 0;
    border-radius: 3px;
    box-shadow: 0 1px 1px rgba(0,0,0,.05);
}
.table-title {        
    padding-bottom: 15px;
    background: #435d7d;
    color: #fff;
    padding: 16px 30px;
    margin: -20px -25px 10px;
    border-radius: 3px 3px 0 0;
}
.table-title h2 {
    margin: 5px 0 0;
    font-size: 24px;
}
.table-title .btn-group {
    float: right;
}
.table-title .btn {
    color: #fff;
    float: right;
    font-size: 13px;
    border: none;
    min-width: 50px;
    border-radius: 2px;
    border: none;
    outline: none !important;
    margin-left: 10px;
}
.table-title .btn i {
    float: left;
    font-size: 21px;
    margin-right: 5px;
}
.table-title .btn span {
    float: left;
    margin-top: 2px;
}
table.table tr th, table.table tr td {
    border-color: #e9e9e9;
    padding: 5px;
    vertical-align: middle;
}
table.table-striped tbody tr:nth-of-type(odd) {
    background-color: #fcfcfc;
}
table.table-striped.table-hover tbody tr:hover {
    background: #f5f5f5;
}
table.table th i {
    font-size: 13px;
    margin: 0 5px;
    cursor: pointer;
}	
table.table td:last-child i {
    opacity: 0.9;
    font-size: 22px;
    margin: 0 5px;
}
table.table td a {
    font-weight: bold;
    color: #566787;
    display: inline-block;
    text-decoration: none;
    outline: none !important;
}
table.table td a:hover {
    color: #2196F3;
}
table.table td a.quedit, table.table td a.qaedit {
    color: #FFC107;
}
table.table td a.qudelete, table.table td a.qadelete {
    color: #F44336;
}
table.table td i {
    font-size: 19px;
}
table.table .avatar {
    border-radius: 50%;
    vertical-align: middle;
    margin-right: 10px;
}
table{
    table-layout: fixed;
    width: 100%;
}
th,td {
    word-wrap: break-word;
}
.pagination {
    float: right;
    margin: 0 0 5px;
}
.pagination li a {
    border: none;
    font-size: 13px;
    min-width: 30px;
    min-height: 30px;
    color: #999;
    margin: 0 2px;
    line-height: 30px;
    border-radius: 2px !important;
    text-align: center;
    padding: 0 6px;
}
.pagination li a:hover {
    color: #666;
}	
.pagination li.active a, .pagination li.active a.page-link {
    background: #03A9F4;
}
.pagination li.active a:hover {        
    background: #0397d6;
}
.pagination li.disabled i {
    color: #ccc;
}
.pagination li i {
    font-size: 16px;
    padding-top: 6px
}
.hint-text {
    float: left;
    margin-top: 10px;
    font-size: 13px;
}
/* Custom checkbox */
.custom-checkbox {
    position: relative;
}
.custom-checkbox input[type="checkbox"] {    
    opacity: 0;
    position: absolute;
    margin: 5px 0 0 3px;
    z-index: 9;
}
.custom-checkbox label:before{
    width: 18px;
    height: 18px;
}
.custom-checkbox label:before {
    content: '';
    margin-right: 10px;
    display: inline-block;
    vertical-align: text-top;
    background: white;
    border: 1px solid #bbb;
    border-radius: 2px;
    box-sizing: border-box;
    z-index: 2;
}
.custom-checkbox input[type="checkbox"]:checked + label:after {
    content: '';
    position: absolute;
    left: 6px;
    top: 3px;
    width: 6px;
    height: 11px;
    border: solid #000;
    border-width: 0 3px 3px 0;
    transform: inherit;
    z-index: 3;
    transform: rotateZ(45deg);
}
.custom-checkbox input[type="checkbox"]:checked + label:before {
    border-color: #03A9F4;
    background: #03A9F4;
}
.custom-checkbox input[type="checkbox"]:checked + label:after {
    border-color: #fff;
}
.custom-checkbox input[type="checkbox"]:disabled + label:before {
    color: #b8b8b8;
    cursor: auto;
    box-shadow: none;
    background: #ddd;
}
/* Modal styles */
.modal .modal-header, .modal .modal-body, .modal .modal-footer {
    padding: 20px 30px;
}
.modal .modal-content {
    border-radius: 3px;
}
.modal .modal-footer {
    background: #ecf0f1;
    border-radius: 0 0 3px 3px;
}
.modal .modal-title {
    display: inline-block;
}
.modal .form-control {
    border-radius: 2px;
    box-shadow: none;
    border-color: #dddddd;
}
.modal textarea.form-control {
    resize: vertical;
}
.modal .btn {
    border-radius: 2px;
    min-width: 100px;
}	
.modal form label {
    font-weight: normal;
}
</style>