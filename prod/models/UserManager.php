<?php

require_once "../core/Model.php";

class UserManager extends Model{
    function getAll(){
        $db = $this->dbConnect();
        $req = $db->query("SELECT * FROM Utilisateur");
        return $req->fetchAll(PDO::FETCH_ASSOC);
    }

    function getFromLogin($login){
        $db = $this->dbConnect();
        $req = $db->prepare("SELECT * FROM Utilisateur WHERE login=?");
        $req->execute(array($login));
        return $req->fetchAll(PDO::FETCH_ASSOC);
    }

    function addUser($login, $password, $fname, $lname, $email){
        $db = $this->dbConnect();
        $req = $db->prepare("INSERT INTO Utilisateur(login, password, prenom, nom, email) VALUES (?, ?, ?, ?, ?)");
        $password = password_hash($password, PASSWORD_BCRYPT);
        if($req->execute(array($login, $password, $fname, $lname, $email)))
            return true;
        else{
            var_dump($db->errorInfo());
            return false;
        }
    }

    function editUser($login, $password, $fname, $lname, $email){
        $db = $this->dbConnect();
        $req = $db->prepare("UPDATE Utilisateur SET password = ?, prenom = ?, nom = ?, email = ? WHERE login = '" . $login. "'");
        $password = password_hash($password, PASSWORD_BCRYPT);
        if($req->execute(array($password, $fname, $lname, $email)))
            return true;
        else{
            var_dump($db->errorInfo());
            return false;
        }
    }

    function deleteUser($login){
        $db = $this->dbConnect();
        return $db->query("DELETE FROM Utilisateur WHERE login = '" . $login. "'");
    }
}

?>