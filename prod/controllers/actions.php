<?php

require_once "../core/Controller.php";

// Get model dependencies
require_once "../models/ActionManager.php";
$actions = new ActionManager();

if(isset($id)){ // Edit or delete
    if($action=="edit"){
        if(!empty($_POST)){
            extract($_POST);
            if(empty($titre) || empty($date) || empty($lieu)){
                putError("Titre, date et lieu obligatoires !");
                die();
            }
            if($actions->editAction($id, $titre, $lieu, $date, $debut, $fin))
                putSuccess("Action modifiée !<br><small>Rechargez pour actualiser l'affichage</small>");
            else
                putError("Erreur lors de la modification");
        } else {
            putError("Erreur");
        }
    } else if($action=="delete"){
        if($actions->deleteAction($id))
            putSuccess("Action supprimée !<br><small>Rechargez pour actualiser l'affichage</small>");
        else
            putError("Erreur lors de la suppression");
    }
}else{
    // Add action
    if(!empty($_POST)){
        extract($_POST);
        if(empty($titre) || empty($date) || empty($lieu)){
            putError("Titre, date et lieu obligatoires !");
            die();
        }
        if($actions->addAction($titre, $lieu, $date, $debut, $fin))
            putSuccess("Action ajoutée !<br><small>Rechargez pour actualiser l'affichage</small>");
        else
            putError("Erreur lors de l'ajout");
    } else {
        // Get view
        $allActions = $actions->getAll();
        require_once "../views/actions.php";
    }
}

?>