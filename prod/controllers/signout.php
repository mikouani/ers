<?php
session_start();
unset($_SESSION['auth']);
session_destroy();
?>
<script type="text/javascript">
window.location.href = '<?= $GLOBALS['basePath'] ?>';
</script>