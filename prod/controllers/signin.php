<?php

require_once "../core/Controller.php";

// Get user model
require_once "../models/UserManager.php";
$users = new UserManager();

if(!empty($_POST)){
    $user = $users->getFromLogin($_POST['login']);
    if(!empty($user)){
        if(password_verify($_POST['password'], $user[0]['password'])){
            session_start();
            $_SESSION['auth']=$user[0];
            header("Location:/admin");
            exit();
        } else
            $error="Mauvais mot de passe !";
    } else
        $error="Mauvais login !";
}

require_once "../views/signin.php";
?>