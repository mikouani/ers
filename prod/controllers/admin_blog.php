<?php

require_once "../core/Controller.php";

// Get model dependencies
require_once "../models/UserManager.php";
require_once "../models/CategoryManager.php";
require_once "../models/BlogManager.php";
$userM = new UserManager();
$cateogryM = new CategoryManager();
$blog = new BlogManager();

if(isset($post)){ // Edit or delete
    if($action=="edit"){
        if(!empty($_POST)){
            extract($_POST);
            if(isset($recette) && (empty($pers) || empty($cuiss) || empty($prep) || intval($pers)<1)){
                putError("Infos de recette non-remplies !");
                die();
            } else if(empty($titre) || empty($contenu)){
                putError("Titre ou contenu vide !");
                die();
            }
            if(!isset($ressource)) $ressource=0; else $ressource=1;

            if((!empty($_FILES['miniature']["tmp_name"]) && !checkImage($_FILES['miniature'])) || (!empty($_FILES['couv']['tmp_name']) && !checkImage($_FILES['couv'])) || (!empty($_FILES['illu']['tmp_name']) && !checkImage($_FILES['illu']))){
                putError("Une des images n'est pas valide !");
                die();
            }

            if((!isset($recette) && $blog->editPost($post, $cat, $titre, $contenu, $auteur, $ressource)) || (isset($recette) && $blog->editRecette($post, $cat, $titre, $contenu, $auteur, $ressource, $prep, $cuiss, $pers)))
                putSuccess("Post modifié !<br><small>Rechargez pour actualiser l'affichage</small>");
            else
                putError("Erreur lors de la modification");
        } else {
            putError("Erreur");
        }
    } else if($action=="delete"){
        if($blog->deletePost($post))
            putSuccess("Post supprimé !<br><small>Rechargez pour actualiser l'affichage</small>");
        else
            putError("Erreur lors de la suppression");
    }
}else{
    // Add post
    if(!empty($_POST)){
        extract($_POST);
        if(isset($recette) && (empty($pers) || empty($cuiss) || empty($prep) || intval($pers)<1)){
            putError("Infos de recette non-remplies !");
            die();
        } else if(empty($titre) || empty($contenu)){
            putError("Titre ou contenu vide !");
            die();
        }
        if(!isset($ressource)) $ressource=0; else $ressource=1;

        if((!empty($_FILES['miniature']["tmp_name"]) && !checkImage($_FILES['miniature'])) || (!empty($_FILES['couv']['tmp_name']) && !checkImage($_FILES['couv'])) || (!empty($_FILES['illu']['tmp_name']) && !checkImage($_FILES['illu']))){
            putError("Une des images n'est pas valide !");
            die();
        }

        if((!isset($recette) && $blog->addPost($cat, $titre, $contenu, $auteur, $ressource)) || (isset($recette) && $blog->addRecette($cat, $titre, $contenu, $auteur, $ressource, $pers, $cuiss, $prep)))
            putSuccess("Post ajouté !<br><small>Rechargez pour actualiser l'affichage</small>");
        else
            putError("Erreur lors de l'ajout");
    } else {
        // Get view
        $users = $userM->getAll();
        $categories = $cateogryM->getAll();
        $posts = $blog->get10($page);
        $count = $blog->getCount()[0];
        $max = ceil($count/10);
        require_once "../views/admin_blog.php";
    }
}

?>