<?php

require_once "../core/Controller.php";

// Get model dependencies
require_once "../models/UserManager.php";
$users = new UserManager();

if(isset($user)){ // Edit or delete
    if($action=="edit"){
        if(!empty($_POST)){
            extract($_POST);
            if($users->editUser($user, $password, $fname, $lname, $email))
                putSuccess("Utilisateur modifié !<br><small>Rechargez pour actualiser l'affichage</small>");
            else
                putError("Erreur lors de la modification");
        } else {
            putError("Erreur");
        }
    } else if($action=="delete"){
        if($users->deleteUser($user))
            putSuccess("Utilisateur supprimé !<br><small>Rechargez pour actualiser l'affichage</small>");
        else
            putError("Erreur lors de la suppression");
    }
}else{
    // Add user
    if(!empty($_POST)){
        extract($_POST);
        if($users->addUser($login, $password, $fname, $lname, $email))
            putSuccess("Utilisateur ajouté !<br><small>Rechargez pour actualiser l'affichage</small>");
        else
            putError("Erreur lors de l'ajout");
    } else {
        // Get view
        $allUsers = $users->getAll();
        require_once "../views/users.php";
    }
}

?>