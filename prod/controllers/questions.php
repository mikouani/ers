<?php

require_once "../core/Controller.php";

// Get model dependencies
require_once "../models/QuestionManager.php";
$manager = new QuestionManager();

if(isset($id)){ // Edit or delete
    if($action=="edit"){
        if(!empty($_POST)){
            extract($_POST);
            if(empty($reponse)){
                putError("Reponse vide !");
                die();
            }
            if($manager->editQuestion($id, $intitule, $reponse))
                putSuccess("Question modifiée !<br><small>Rechargez pour actualiser l'affichage</small>");
            else
                putError("Erreur lors de la modification");
        } else {
            putError("Erreur");
        }
    } else if($action=="delete"){
        if($manager->deleteQuestion($id))
            putSuccess("Question supprimée !<br><small>Rechargez pour actualiser l'affichage</small>");
        else
            putError("Erreur lors de la suppression");
    }
}else{
    // Get view
    $answered = $manager->getAnswered(10, $page);
    $unanswered = $manager->getUnanswered(10, $page);
    $countA = $manager->getCountAnswered()[0];
    $countU = $manager->getCountUnanswered()[0];
    $maxA = ceil($countA/10);
    $maxU = ceil($countU/10);

    require_once "../views/questions.php";
}

?>