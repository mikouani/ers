<?php if(session_status()===PHP_SESSION_NONE) session_start(); if(!isset($_SESSION['auth'])) header('Location:admin/signin'); ?>
<!doctype html>
<html lang="fr">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
        <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
        <link rel="icon" type="image/png" href="<?= $GLOBALS['basePath'] ?>/rsc/logo.png" />
        <title>ERS | <?= ucfirst(substr($match['name'], 5)) ?></title>
    </head>
    <body>
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
        <a class="navbar-brand" href="<?= $router->generate("back-dashboard"); ?>"><img src="<?= $GLOBALS['basePath'] ?>/rsc/logo.png" height=48></a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav mr-auto">
                <li class="nav-item"><a class="nav-link" href="<?= $router->generate("back-blog", array("page"=>1)) ?>">Articles</a></li>
                <li class="nav-item"><a class="nav-link" href="<?= $router->generate("back-actions") ?>">Actions</a></li>
                <li class="nav-item"><a class="nav-link" href="<?= $router->generate("back-questions", array("page"=>1)) ?>">Boîte à question</a></li>
                <li class="nav-item"><a class="nav-link" href="<?= $router->generate("back-utilisateurs") ?>">Utilisateurs</a></li>
            </ul>
            <a class="nav-link" style="color:rgba(255,255,255,.5)" href="<?= $router->generate("back-déconnexion") ?>">Déconnexion</a>
        </div>
    </nav>