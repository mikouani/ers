<div class="col p-4">
    <h1>Bienvenue</h1>
    <p>Bienvenue sur la page des Étudiants Relais Santé de l’UTC! Nous sommes une équipe d’étudiants formés aux problématiques de la santé rattachés au Service de Médecine Préventive.</p>
    <h1>Actualités</h1>
    <div class="container">
        <div class="row">
            <div class="col-md border-right">
                <h2>Prochaines actions</h2>
                <ul class="list-group list-group-flush">
                    <?php
                        foreach($data as $k=>$d){
                            if($d){
                                echo "<li class='p-2 list-group-item ";
                                if($k==count($data)-1) 
                                    echo "border-0";
                                echo "'><span class='font-weight-bold'>".$d["titre"]."</span> (<span class='font-italic'>".$d["lieu"]."</span>)<br><small> ".date("d/m/y", strtotime($d["date"]))." de ".substr($d["debut"],0,-3)." à ". substr($d["fin"],0,-3)."</small></li>";
                            }
                        }
                    ?>
                </ul>
            </div>
            <div class="col-md">
                <h2>Derniers articles</h2>
                <ul class="list-group list-group-flush">
                <?php
                        foreach($blog as $k=>$b){
                            if($b){
                                echo "<li class='p-2 list-group-item ";
                                if($k==count($blog)-1) 
                                    echo "border-0";
                                echo "'><small class='text-right d-block float-right px-2 m-0 border'>".$b["cnom"]."</small><a class='text-left d-block' href='".$GLOBALS['router']->generate("Article", array('id'=>$b['id']))."'>".$b["titre"]."</a><small>".$b["unom"]."</small></li>";
                            }
                        }
                    ?>
                </ul>
            </div>
        </div>
    </div>
    <h1 class="mt-2">Notre mission</h1>
    <p>Notre mission : <i>écouter</i>, <i>informer</i> et <i>prévenir</i> les étudiants.</p>
    <p>Nous recueillons des informations sur la qualité de vie des étudiants, sur leurs difficultés et leurs préoccupations. Nous discutons de pair à pair autour des situations pouvant entraîner des prises de risque. Notre but est ensuite de transmettre des messages de prévention, répondre aux interrogations des étudiants et les aider à s’informer sur tous les sujets liés à la santé : sommeil, stress, addiction, sexualité́, dépistage, alcool, sport et alimentation entre autres.</p>
    <p>Nous sommes également là pour passer le relais à un personnel médical ou à un organisme si cela s’avère nécessaire.</p>
    <h1>Nos outils</h1>
    <p>Afin d’accomplir notre mission nous animons une page Facebook (<i>Étudiants Relais Santé UTC</i>) où nous partageons les informations relatives à tous les thèmes traités, les actions menées. Nous sommes également disponibles pour discuter et répondre à vos questions sur notre page.</p>
    <p>Les étudiants relais santé ont tous reçu des formations aux thèmes et aux méthodes de prévention de santé lors de sessions organisées par le Service de médecine préventive.</p>
    <p>Nous menons des projets d’information ou prévention et organisons des actions de prévention dans le cadre de la vie universitaire. Pour plus d’informations, suis-nous sur notre page Facebook et consulte régulièrement notre site !</p>
</div>
