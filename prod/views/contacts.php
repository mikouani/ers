<div class="col p-4">
    <div class="row">
        <div class="col text-center my-4">
            <h4>Contactez nous par mail</h4>
            <h1>ers@utc.fr</h1>
            <h4>ou via nos réseaux en bas de page</h4>
        </div>
    </div>
    <div class="row bg-light text-justify" id="content">
        <div class="col-lg-6 p-4 border-right">
            <h2>Service Médecine Préventive</h2>
            <p>Passer par cette adresse pour vous adresser au médecin, à l'infirmière et à la psychologue de l'UTC :</p>
            <h5 class="text-center">medecine-preventive@utc.fr</h5>

            <h2>Proche de vous</h2>
            <ul>
                <li>
                    <span class="font-weight-bold">Centre gratuit d'information, de dépistage et de diagnostic (CeGIDD) de Compiègne : </span><br>
                    03 44 23 63 12 - <a href="http://www.ch-compiegnenoyon.fr/annuaire-0-3.html">Site Web</a><br>
                    <small>Consultation sans RDV à l'hôpital de Compiègne (horaires sur leur site)</small><br>
                </li>
                <li>
                    <span class="font-weight-bold">Service d'Aide aux Toxicomanes (SATO) de Compiègne : </span><br>
                    03 44 40 08 77 - <a href="http://www.sato-picardie.fr/">Site web</a><br>
                    <small>16 Avenue des Martyrs de la Liberté, 60200 Compiègne</small>
                </li>
                <li>
                    <span class="font-weight-bold">Association nationale de prévention en Alcoologie et Addictologie (ANPAA) à Compiègne : </span><br>
                    03 44 20 51 35<br>
                    <small>20 Rue du Fonds Pernant, 60200 Compiègne</small>
                </li>
                <li>
                    <span class="font-weight-bold">Centre de planification et d'éducation familiale (CPEF) de Compiègne : </span><br>
                    03 44 10 40 40<br>
                    <small>1 Place de la Croix Blanche, 60200 Compiègne</small>
                </li>
            </ul>
            <h2>Autre</h2>
            <ul>
                <li>
                    <span class="font-weight-bold">Maladies Rares Info Services</span><br>
                    0 810 63 19 20 - <a href="http://www.maladiesraresinfo.org/">Site Web</a><br>
                    <small>09h-13h et 14h-18h 7j/7 (Non surtaxé depuis un fixe)</small>
                </li>
            </ul>
        </div>
        <div class="col-lg-6 p-4">
            <h2>Addiction</h2>
            <ul>
                <li>
                    <span class="font-weight-bold">Tabac Info Service</span><br>
                    39 89 - <a href="http://www.tabac-info-service.fr/">Site Web</a><br>
                    <small>08h à 20h du lundi au samedi (0,15€/min)</small>
                </li>
                <li>
                    <span class="font-weight-bold">Alcool Info Service</span><br>
                    0 980 980 930 - <a href="http://www.alcoolinfoservice.fr/">Site Web</a><br>
                    <small>08h à 20h 7j/7 (non surtaxé)</small>
                </li>
                <li>
                    <span class="font-weight-bold">Drogues Info Service</span><br>
                    0 800 23 13 13 (depuis un fixe)<br>
                    01 70 23 13 13 (depuis un portable)<br>
                    <a href="http://www.drogues-info-service.fr/">Site Web</a><br>
                    <small>08h à 20h 7j/7 (non surtaxé)</small><br>
                </li>
                <li>
                    <span class="font-weight-bold">Ecoute Cannabis</span><br>
                    0 980 980 940<br>
                    <small>08h à 20h 7j/7 (non surtaxé)</small><br>
                </li>
                <li>
                    <span class="font-weight-bold">Joueurs Info Service</span><br>
                    09 74 75 13 13 - <a href="http://www.joueurs-info-service.fr/">Site Web</a><br>
                    <small>08h à 20h 7j/7 (non surtaxé)</small>
                </li>
            </ul>
            <h2>Sexualité</h2>
            <ul>
                <li>
                    <span class="font-weight-bold">Sida Info Service</span><br>
                    0 800 840 800 - <a href="http://www.sida-info-service.org/">Site Web</a><br>
                    <small>24h/24 7j/7 (gratuit depuis un fixe)</small>
                </li>
                <li>
                    <span class="font-weight-bold">Ligne Azur</span><br>
                    Ecoute, information, soutien et orientation pour toute question sur son orientation sexuelle<br>
                    0 800 840 800 - <a href="http://www.ligneazur.org/">Site Web</a><br>
                    <small>08h à 23h 7j/7 (0,06€/min + coût d'un appel local)</small>
                </li>
            </ul>
            <h2>Bien-être</h2>
            <ul>
                <li>
                    <span class="font-weight-bold">SOS Suicide Phénix</span><br>
                    01 40 44 46 45 - <small>12h à 24h 7j/7 (0,15€/min depuis un fixe)</small><br>
                    0 825 120 364 - <small>16h à 20h 7j/7 (0,15€/min depuis un fixe)</small><br>
                    <a href="http://www.sos-suicide-phenix.org/">Site Web</a><br>
                </li>
                <li>
                    <span class="font-weight-bold">SOS Amitié France</span><br>
                    50 postes d'écoute, liste disponble sur leur <a href="http://www.sos-amitie.org/">Site Web</a><br>
                </li>
            </ul>
        </div>
    </div>
</div>