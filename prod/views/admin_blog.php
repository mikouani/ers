<div class="container">
    <div class="row">
        <div class="col text-center align-self-center p-4">
        <div id="loading"><img src="<?= $GLOBALS['basePath'] ?>/rsc/loading.gif"></div>
        <div class="table-wrapper">
            <div class="table-title">
                <div class="row">
                    <div class="col-sm-6">
						<h2>Gestion Posts</h2>
					</div>
					<div class="col-sm-6">
						<a href="#addPostModal" class="btn btn-success" data-toggle="modal"><i class="material-icons">&#xE147;</i> <span>Nouveau post</span></a>
					</div>
                </div>
            </div>
            <table class="table table-striped table-hover">
                <col>
                <col>
                <col>
                <col>
                <col width=75>
                <thead>
                    <tr>
                        <th>Catégorie</th>
                        <th>Titre</th>
						<th>Posté le</th>
                        <th>Auteur</th>
                        <th>Actions</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                        foreach($posts as $p){
                            ?>
                            <tr>
                                <td><?= $p['cnom'] ?></td>
                                <td><?= $p['titre'] ?></td>
                                <td><?= date('Y/m/d | H:i', strtotime($p['date'])) ?></td>
                                <td><?= $p['unom'] ?></td>
                                <td>
                                    <a href="#editPostModal" class="edit" id="<?= $p['id'] ?>" data-toggle="modal"><i class="material-icons m-0" data-toggle="tooltip" title="Modifier">&#xE254;</i></a>
                                    <a href="#deleteConfirm" class="delete" id="<?= $p['id'] ?>" data-toggle="modal"><i class="material-icons m-0" data-toggle="tooltip" title="Supprimer">&#xE872;</i></a>
                                </td>
                            </tr>
                            <?php
                        }
                    ?>
                </tbody>
            </table>
			<div class="clearfix">
                <div class="hint-text">Affichage de <b>10</b> posts sur <b><?= $count ?></b></div>
                <ul class="pagination">
                    <li class="page-item <?php if($page==1){ echo "disabled"; } ?>"><a href="<?= $GLOBALS['router']->generate("back-blog", array('page'=>$page-1)) ?>" class="page-link">Précédant</a></li>
                    <?php
                        for($i=$page-2;$i<=$page+2;$i++){
                            if($i>0 && $i<=$max){
                                echo '<li class="page-item';
                                if($i==$page) echo " active";
                                echo '"><a href="#" class="page-link">'.$i.'</a></li>';
                            }
                        }
                    ?>
                    <li class="page-item <?php if($page==$max){ echo "disabled"; }?>"><a href="<?= $GLOBALS['router']->generate("back-blog", array('page'=>$page+1)) ?>" class="page-link">Suivant</a></li>
                </ul>
            </div>
        </div>
        <div class="text-left">
            <h1 class="text-center">Mise en forme des articles</h1>
            <p class="text-center">Mise en forme des articles avec markdown, une syntaxe spécifique pour facilement mettre en forme du texte. <a href="https://www.markdownguide.org/basic-syntax/">Comment ça marche ?</a></p>
            <h2>Ajouter une vidéo YouTube</h2>
            <p>Pour ajouter une vidéo à un article, il faut ajouter une image avec un lien vers la vidéo. Pour utiliser la miniature de la vidéo, copier/coller ce code en remplaçant "YOUTUBE_VIDEO_ID_HERE" par l'ID de la vidéo (numéro à la fin du lien de la vidéo youtube en question) :</p>
            <p style="overflow-wrap:break-word">[![Vidéo](https://img.youtube.com/vi/YOUTUBE_VIDEO_ID_HERE/0.jpg)](https://www.youtube.com/watch?v=YOUTUBE_VIDEO_ID_HERE)</p>
        </div>
    </div>
</div>
<!-- Add Modal HTML -->
<div id="addPostModal" class="modal fade">
    <div class="modal-dialog modal-xl">
        <div class="modal-content">
            <form method="post" enctype="multipart/form-data">
                <div class="modal-header">						
                    <h4 class="modal-title">Ajouter un post</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                </div>
                <div class="modal-body">					
                    <div class="form-group">
                        <label for="categorie">Catégorie</label>
                        <select class="custom-select" name="cat" id="categorie">
                        <?php
                            foreach($categories as $c){
                                echo "<option value='".$c['id']."'>".$c['nom']."</option>";
                            }
                        ?>
                        </select>
                    </div>
                    <div class="form-group custom-checkbox text-center">
                        <input type="checkbox" class="form-control" name="ressource" id="ressource">
                        <label for="ressource">Ressource</label>
                    </div>
                    <div class="form-group custom-checkbox text-center dRecette">
                        <input type="checkbox" class="form-control" name="recette" id="recette">
                        <label for="recette">Recette</label>
                    </div>
                    <div class="row">
                        <div class="col-md">
                            <div class="form-group">
                                <label for="prep">Temps de préparation (hh:mm)</label>
                                <input type="time" class="form-control" name="prep" id="prep">
                            </div>                        </div>
                        <div class="col-md">
                            <div class="form-group">
                                <label for="cuiss">Temps de cuisson (hh:mm)</label>
                                <input type="time" class="form-control" name="cuiss" id="cuiss">
                            </div>
                        </div>
                        <div class="col-md">
                            <div class="form-group">
                                <label for="pers">Personnes</label>
                                <input type="number" class="form-control" name="pers" id="pers" min=1 step=1>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="titre">Titre</label>
                        <input type="text" class="form-control" name="titre" id="titre" required>
                    </div>
                    <div class="form-group">
                        <label for="contenu">Contenu</label>
                        <textarea class="form-control" name="contenu" id="contenu" required rows=8></textarea>
                    </div>
                    <div class="form-group">
                        <label for="auteur">Auteur</label>
                        <select class="custom-select" name="auteur" id="auteur">
                        <?php
                            foreach($users as $u){
                                echo "<option value='".$u['login']."'>".$u['prenom']." ".$u['nom']."</option>";
                            }
                        ?>
                        </select>
                    </div>
                    <div class="row">
                    <div class="col-xl">
                        <div class="form-group">
                            <label>Miniature (300x300)</label>
                            <div class="custom-file">
                                <input type="file" name="miniature" class="custom-file-input" id="miniature">
                                <label class="custom-file-label" for="miniature" data-browse="Parcourir">Importer une photo</label>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl">
                        <div class="form-group">
                            <label>Photo de couverture (1200x300)</label>
                            <div class="custom-file">
                                <input type="file" name="couv" class="custom-file-input" id="couv">
                                <label class="custom-file-label" for="couv" data-browse="Parcourir">Importer une photo</label>
                            </div>
                        </div>				
                    </div>
                    <div class="col-xl">
                        <div class="form-group">
                            <label>Photo illustration :</label>
                            <div class="custom-file">
                                <input type="file" name="illu" class="custom-file-input" id="illu">
                                <label class="custom-file-label" for="illu" data-browse="Parcourir">Importer une photo</label>
                            </div>
                        </div>				
                    </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <input type="button" class="btn btn-default" data-dismiss="modal" value="Annuler">
                    <input type="button" class="btn btn-success" data-dismiss="modal" id="addPostBtn" value="Ajouter">
                </div>
            </form>
        </div>
    </div>
</div>
<!-- Edit Modal HTML -->
<div id="editPostModal" class="modal fade">
    <div class="modal-dialog modal-xl">
        <div class="modal-content">
            <form method="post" enctype="multipart/form-data">
                <div class="modal-header">						
                    <h4 class="modal-title">Modifier le post</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                </div>
                <div class="modal-body">					
                    <div class="form-group">
                        <label for="ecategorie">Catégorie</label>
                        <select class="custom-select" name="cat" id="ecategorie">
                        <?php
                            foreach($categories as $c){
                                echo "<option value='".$c['id']."'>".$c['nom']."</option>";
                            }
                        ?>
                        </select>
                    </div>
                    <div class="form-group custom-checkbox text-center">
                        <input type="checkbox" class="form-control" name="ressource" id="eressource">
                        <label for="eressource">Ressource</label>
                    </div>
                    <div class="form-group custom-checkbox text-center dRecette">
                        <input type="checkbox" class="form-control" name="recette" id="erecette">
                        <label for="erecette">Recette</label>
                    </div>
                    <div class="row">
                        <div class="col-md">
                            <div class="form-group">
                                <label for="eprep">Temps de préparation (hh:mm)</label>
                                <input type="time" class="form-control" name="prep" id="eprep">
                            </div>                        </div>
                        <div class="col-md">
                            <div class="form-group">
                                <label for="ecuiss">Temps de cuisson (hh:mm)</label>
                                <input type="time" class="form-control" name="cuiss" id="ecuiss">
                            </div>
                        </div>
                        <div class="col-md">
                            <div class="form-group">
                                <label for="epers">Personnes</label>
                                <input type="number" class="form-control" name="pers" id="epers" min=1 step=1>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="etitre">Titre</label>
                        <input type="text" class="form-control" name="titre" id="etitre" required>
                    </div>
                    <div class="form-group">
                        <label for="econtenu">Contenu</label>
                        <textarea class="form-control" name="contenu" id="econtenu" required rows=8></textarea>
                    </div>
                    <div class="form-group">
                        <label for="eauteur">Auteur</label>
                        <select class="custom-select" name="auteur" id="eauteur">
                        <?php
                            foreach($users as $u){
                                echo "<option value='".$u['login']."'>".$u['prenom']." ".$u['nom']."</option>";
                            }
                        ?>
                        </select>
                    </div>
                    <div class="row">
                    <div class="col-xl">
                        <div class="form-group">
                            <label>Miniature (300x300)</label>
                            <div class="custom-file">
                                <input type="file" name="miniature" class="custom-file-input" id="eminiature" required>
                                <label class="custom-file-label" for="eminiature" data-browse="Parcourir">Importer une photo</label>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl">
                        <div class="form-group">
                            <label>Photo de couverture (1200x300)</label>
                            <div class="custom-file">
                                <input type="file" name="couv" class="custom-file-input" id="ecouv" required>
                                <label class="custom-file-label" for="ecouv" data-browse="Parcourir">Importer une photo</label>
                            </div>
                        </div>				
                    </div>
                    <div class="col-xl">
                        <div class="form-group">
                            <label>Photo illustration :</label>
                            <div class="custom-file">
                                <input type="file" name="illu" class="custom-file-input" id="eillu">
                                <label class="custom-file-label" for="eillu" data-browse="Parcourir">Importer une photo</label>
                            </div>
                        </div>				
                    </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <input type="button" class="btn btn-default" data-dismiss="modal" value="Annuler">
                    <input type="button" class="btn btn-info" data-dismiss="modal" id="editPostBtn" value="Enregistrer">
                </div>
            </form>
        </div>
    </div>
</div>
<!-- Delete Modal HTML -->
<div id="deleteConfirm" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">						
                <h4 class="modal-title">Supprimer le post</h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            </div>
            <div class="modal-body">					
                <p>Etes-vous sur de vouloir supprimer ce post ?</p>
                <p class="text-warning"><small>L'action est irréversible</small></p>
            </div>
            <div class="modal-footer">
                <input type="button" class="btn btn-default" data-dismiss="modal" value="Annuler">
                <input type="button" id="delPostBtn" data-dismiss="modal" class="btn btn-danger" value="Supprimer">
            </div>
        </div>
    </div>
</div>
<style>
.table-wrapper {
    background: #fff;
    margin: 30px 0;
    border-radius: 3px;
    box-shadow: 0 1px 1px rgba(0,0,0,.05);
}
.table-title {        
    padding-bottom: 15px;
    background: #435d7d;
    color: #fff;
    padding: 16px 30px;
    margin: -20px -25px 10px;
    border-radius: 3px 3px 0 0;
}
.table-title h2 {
    margin: 5px 0 0;
    font-size: 24px;
}
.table-title .btn-group {
    float: right;
}
.table-title .btn {
    color: #fff;
    float: right;
    font-size: 13px;
    border: none;
    min-width: 50px;
    border-radius: 2px;
    border: none;
    outline: none !important;
    margin-left: 10px;
}
.table-title .btn i {
    float: left;
    font-size: 21px;
    margin-right: 5px;
}
.table-title .btn span {
    float: left;
    margin-top: 2px;
}
table.table tr th, table.table tr td {
    border-color: #e9e9e9;
    padding: 5px;
    vertical-align: middle;
}
table.table-striped tbody tr:nth-of-type(odd) {
    background-color: #fcfcfc;
}
table.table-striped.table-hover tbody tr:hover {
    background: #f5f5f5;
}
table.table th i {
    font-size: 13px;
    margin: 0 5px;
    cursor: pointer;
}	
table.table td:last-child i {
    opacity: 0.9;
    font-size: 22px;
    margin: 0 5px;
}
table.table td a {
    font-weight: bold;
    color: #566787;
    display: inline-block;
    text-decoration: none;
    outline: none !important;
}
table.table td a:hover {
    color: #2196F3;
}
table.table td a.edit {
    color: #FFC107;
}
table.table td a.delete {
    color: #F44336;
}
table.table td i {
    font-size: 19px;
}
table.table .avatar {
    border-radius: 50%;
    vertical-align: middle;
    margin-right: 10px;
}
table{
    table-layout: fixed;
    width: 100%;
}
th,td {
    word-wrap: break-word;
}
.pagination {
    float: right;
    margin: 0 0 5px;
}
.pagination li a {
    border: none;
    font-size: 13px;
    min-width: 30px;
    min-height: 30px;
    color: #999;
    margin: 0 2px;
    line-height: 30px;
    border-radius: 2px !important;
    text-align: center;
    padding: 0 6px;
}
.pagination li a:hover {
    color: #666;
}	
.pagination li.active a, .pagination li.active a.page-link {
    background: #03A9F4;
}
.pagination li.active a:hover {        
    background: #0397d6;
}
.pagination li.disabled i {
    color: #ccc;
}
.pagination li i {
    font-size: 16px;
    padding-top: 6px
}
.hint-text {
    float: left;
    margin-top: 10px;
    font-size: 13px;
}
/* Custom checkbox */
.custom-checkbox {
    position: relative;
}
.custom-checkbox input[type="checkbox"] {    
    opacity: 0;
    position: absolute;
    margin: 5px 0 0 3px;
    z-index: 9;
}
.custom-checkbox label:before{
    width: 18px;
    height: 18px;
}
.custom-checkbox label:before {
    content: '';
    margin-right: 10px;
    display: inline-block;
    vertical-align: text-top;
    background: white;
    border: 1px solid #bbb;
    border-radius: 2px;
    box-sizing: border-box;
    z-index: 2;
}
.custom-checkbox input[type="checkbox"]:checked + label:after {
    content: '';
    position: absolute;
    left: 6px;
    top: 3px;
    width: 6px;
    height: 11px;
    border: solid #000;
    border-width: 0 3px 3px 0;
    transform: inherit;
    z-index: 3;
    transform: rotateZ(45deg);
}
.custom-checkbox input[type="checkbox"]:checked + label:before {
    border-color: #03A9F4;
    background: #03A9F4;
}
.custom-checkbox input[type="checkbox"]:checked + label:after {
    border-color: #fff;
}
.custom-checkbox input[type="checkbox"]:disabled + label:before {
    color: #b8b8b8;
    cursor: auto;
    box-shadow: none;
    background: #ddd;
}
/* Modal styles */
.modal .modal-header, .modal .modal-body, .modal .modal-footer {
    padding: 20px 30px;
}
.modal .modal-content {
    border-radius: 3px;
}
.modal .modal-footer {
    background: #ecf0f1;
    border-radius: 0 0 3px 3px;
}
.modal .modal-title {
    display: inline-block;
}
.modal .form-control {
    border-radius: 2px;
    box-shadow: none;
    border-color: #dddddd;
}
.modal textarea.form-control {
    resize: vertical;
}
.modal .btn {
    border-radius: 2px;
    min-width: 100px;
}	
.modal form label {
    font-weight: normal;
}
</style>