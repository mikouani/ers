<?php use Michelf\Markdown; ?>
<div class="col p-4">
  <h1 class="mt-4 mb- text-left"><?= $post['titre'] ?></h1>
  <p class="lead mb-0">
    par <?= $post['unom'] ?>
  </p>

  <!-- Date/Time -->
  <p style="font-size:0.8em">Posté le <?= strftime('%m %B %Y à %H:%M', strtotime($post['date'])); ?></p>
  <hr>

  <!-- Preview Image -->
  <?php
      $img = glob("./posts_img/couv_" . $id . ".*");
      if(!empty($img))
        echo '<img class="img-fluid rounded" src="' . $GLOBALS['basePath'] . $img[0] . '" width=1200 height=300><hr />';
  ?>
  <?php if($recette){ ?>
  <div class="row">
    <div class="col"><span class="font-weight-bold">Temps de préparation :</span> <?= date("H:i", strtotime($post["tempsPrep"])) ?> (h:m)</div>
    <div class="col"><span class="font-weight-bold">Temps de cuisson :</span> <?= date("H:i", strtotime($post["tempsCuisson"])) ?> (h:m)</div>
    <div class="col"><span class="font-weight-bold">Nombre de personne<?php if($post["nbPersonnes"]>1) echo "s" ?> :</span> <?= $post["nbPersonnes"] ?></div>
  </div>
  <hr>
  <?php } ?>

  <!-- Post Content -->
  <div id="blogpostContent">
    <?= Markdown::defaultTransform($post['contenu']) ?>
  </div>
  <?php
      $img = glob("./posts_img/illu_" . $id . ".*");
      if(!empty($img))
          echo '<hr><div class="text-center"><img class="img-fluid rounded" src="' . $GLOBALS['basePath'].$img[0].'"></div>';
  ?>
</div>
<script type="text/javascript">document.title = "ERS | <?= $post["titre"] ?>";</script>
