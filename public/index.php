<?php

setlocale(LC_TIME, "fr_FR.UTF8");

// Setup AltoRouter
require_once "../vendor/autoload.php";
$router = new AltoRouter();

error_reporting(-1);

$basePath = "http://".$_SERVER['SERVER_NAME'];
$publicName = "public";

// Mapping
$router->map("GET", "/", function(){ require_once "../controllers/home.php"; }, 'accueil');
$router->map("GET", "/blog/alcool/[i:page]", function($page){ require_once "../controllers/blog.php"; }, 'alcool');
$router->map("GET", "/blog/tabac/[i:page]", function($page){ require_once "../controllers/blog.php"; }, 'tabac');
$router->map("GET", "/blog/autre-addictions/[i:page]", function($page){ require_once "../controllers/blog.php"; }, 'autre Addictions');
$router->map("GET", "/blog/sexualite/[i:page]", function($page){ require_once "../controllers/blog.php"; }, 'sexualite');
$router->map("GET", "/blog/sommeil-stress/[i:page]", function($page){ require_once "../controllers/blog.php"; }, 'sommeil & Stress');
$router->map("GET", "/blog/nutrition/[i:page]", function($page){ require_once "../controllers/blog.php"; }, 'nutrition');
$router->map("GET", "/blog/divers/[i:page]", function($page){ require_once "../controllers/blog.php"; }, 'divers');
$router->map("GET", "/article/[i:id]", function($id){ require_once "../controllers/article.php"; }, 'Article');
$router->map("GET", "/boiteaquestions/[i:page]", function($page){ require_once "../controllers/boiteaquestions.php"; }, 'boite à Questions');
$router->map("POST", "/boiteaquestions/[i:page]", function($page){ require_once "../controllers/boiteaquestions.php"; }, 'envoi de Question');
$router->map("GET", "/contacts", function(){ require_once "../views/contacts.php"; }, 'contacts');

$router->map("GET", "/admin", function(){ require_once "../controllers/dashboard.php"; }, 'back-dashboard');
$router->map("GET", "/admin/signin", function(){ require_once "../controllers/signin.php"; }, 'back-connexion');
$router->map("POST", "/admin/signin", function(){ require_once "../controllers/signin.php"; }, 'back-connexionPost');
$router->map("GET", "/admin/signout", function(){ require_once "../controllers/signout.php"; }, 'back-déconnexion');

$router->map("GET", "/admin/utilisateurs", function(){ require_once "../controllers/users.php"; }, 'back-utilisateurs');
$router->map("POST", "/admin/utilisateurs/add", function(){ require_once "../controllers/users.php"; }, 'none-addUser');
$router->map("POST", "/admin/utilisateurs/[edit|delete:action]/[a:user]", function($action, $user){ require_once "../controllers/users.php"; }, 'none-updateUser');

$router->map("GET", "/admin/blog/[i:page]", function($page){ require_once "../controllers/admin_blog.php"; }, "back-blog");
$router->map("POST", "/admin/blog/add", function(){ require_once "../controllers/admin_blog.php"; }, "none-addPost");
$router->map("POST", "/admin/blog/[edit|delete:action]/[i:post]", function($action, $post){ require_once "../controllers/admin_blog.php"; }, "none-updatePost");

$router->map("GET", "/admin/actions", function(){ require_once "../controllers/actions.php"; }, 'back-actions');
$router->map("POST", "/admin/actions/add", function(){ require_once "../controllers/actions.php"; }, 'none-addAction');
$router->map("POST", "/admin/actions/[edit|delete:action]/[i:id]", function($action, $id){ require_once "../controllers/actions.php"; }, 'none-updateAction');

$router->map("GET", "/admin/questions/[i:page]", function($page){ require_once "../controllers/questions.php"; }, 'back-questions');
$router->map("POST", "/admin/questions/[edit|delete:action]/[i:id]", function($action, $id){ require_once "../controllers/questions.php"; }, 'none-updateQuestion');

$router->map("GET", "/admin/bons/[i:page]", function($page){ require_once "../controllers/bons.php"; }, 'back-bons');
$router->map("POST", "/admin/bons/[edit|delete:action]/[i:id]", function($action, $id){ require_once "../controllers/bons.php"; }, 'none-updateBons');

$router->map("POST", "/userData/[a:login]", function($login){ require_once "../controllers/userData.php"; }, 'none-userData');
$router->map("POST", "/articleData/[i:id]", function($id){ require_once "../controllers/articleData.php"; }, 'none-articleData');
$router->map("POST", "/actionData/[i:id]", function($id){ require_once "../controllers/actionData.php"; }, 'none-actionData');
$router->map("POST", "/questionData/[i:id]", function($id){ require_once "../controllers/questionData.php"; }, 'none-questionData');

$match=$router->match();

if(is_array($match) && is_callable($match['target'])){
    if(substr($match['name'], 0, 4) === "back"){
        if(strcmp($match['name'], "back-connexion") !== 0 && strcmp($match['name'], "back-connexionPost") !== 0){
            require_once "../elements/backheader.php";
        }
    }
    else if(substr($match['name'], 0, 4) !== "none")
        require_once "../elements/header.php";

    call_user_func_array($match['target'], $match['params']);

    if(substr($match['name'], 0, 4) === "back")
        require_once "../elements/backfooter.php";
    else if(substr($match['name'], 0, 4) !== "none")
        require_once "../elements/footer.php";
}else{
    $match['name'] = "404";
    require_once "../elements/header.php";
    require_once "../error/404.php";
    require_once "../elements/footer.php";
}

?>