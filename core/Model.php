<?php

class Model{
    protected function dbConnect(){
        $db = new PDO('mysql:host=localhost;dbname=ers;charset=utf8', 'root', 'mysql');
        $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        return $db;
    }
}

?>