<?php
    function putError($error){
        echo "<div class='alert alert-danger'>".$error."</div>";
    }
    function putSuccess($error){
        echo "<div class='alert alert-success'>".$error."</div>";
    }
    function checkImage($file){
        // Check if image file is a actual image or fake image
        $check = getimagesize($file["tmp_name"]);
        if($check !== false)
            return true;
        else
            return false;
    }
?>