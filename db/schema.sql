DROP TABLE Article;
DROP TABLE BonCommande;
DROP TABLE Question;
DROP TABLE Recette;
DROP TABLE Post;
DROP TABLE Categorie;
DROP TABLE Action;
DROP TABLE Utilisateur;

CREATE TABLE Utilisateur(
    login VARCHAR(10) PRIMARY KEY,
    password VARCHAR(255),
    prenom VARCHAR(100),
    nom VARCHAR(100),
    email VARCHAR(100)
);

CREATE TABLE Action(
    id INTEGER PRIMARY KEY AUTO_INCREMENT,
    titre VARCHAR(100),
    lieu VARCHAR(150),
    date DATE,
    debut TIME,
    fin TIME
);

CREATE TABLE Categorie(
    id INTEGER PRIMARY KEY AUTO_INCREMENT,
    nom VARCHAR(100)
);

CREATE TABLE Post(
    id INTEGER AUTO_INCREMENT,
    cat INTEGER,
    utilisateur VARCHAR(10) NOT NULL,
    titre VARCHAR(150),
    contenu TEXT,
    date DATETIME DEFAULT CURRENT_TIMESTAMP,
    ressource TINYINT(1),
    CONSTRAINT PK_Post PRIMARY KEY (id, cat),
    FOREIGN KEY (utilisateur) REFERENCES Utilisateur(login),
    FOREIGN KEY (cat) REFERENCES Categorie(id) ON DELETE CASCADE
);

CREATE TABLE Recette(
    id INTEGER,
    tempsPrep TIME,
    tempsCuisson TIME,
    nbPersonnes INTEGER,
    CONSTRAINT PK_Recette PRIMARY KEY (id),
    FOREIGN KEY (id) REFERENCES Post(id) ON DELETE CASCADE
);

CREATE TABLE Question(
    id INTEGER PRIMARY KEY AUTO_INCREMENT,
    date DATETIME DEFAULT CURRENT_TIMESTAMP,
    intitule VARCHAR(255) NOT NULL,
    reponse TEXT
);

CREATE TABLE BonCommande(
    id INTEGER AUTO_INCREMENT PRIMARY KEY,
    titre VARCHAR(200),
    utilisateur VARCHAR(10) NOT NULL,
    envoye TINYINT,
    passe TINYINT,
    dateAjout DATETIME DEFAULT CURRENT_TIMESTAMP,
    dateEnvoye DATETIME,
    datePasse DATETIME,
    CHECK (NOT (passe=1 AND envoye=0)),
    CHECK (datePasse < dateEnvoye),
    CHECK (dateAjout < datePasse),
    CHECK (dateAjout < dateEnvoye),
    FOREIGN KEY (utilisateur) REFERENCES Utilisateur(login)
);

CREATE TABLE Article(
    id INTEGER AUTO_INCREMENT,
    bon INTEGER,
    titre VARCHAR(200),
    prix FLOAT,
    qte INT,
    CONSTRAINT PK_Article PRIMARY KEY (id, bon),
    FOREIGN KEY (bon) REFERENCES BonCommande(id) ON DELETE CASCADE
);
