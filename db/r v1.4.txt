Utilisateur(#login:string, password: string, prenom:string, nom:string,  email:string)

Action(#id:int, titre: string, lieu:string, date: date, debut: time, fin:time)

Categorie(#id:int, nom:string)
Post(#id:int, #cat=>Categorie(id), utilisateur=>Utilisateur(login), titre:string, contenu:text, ressource:bool)
    avec utilisateur NOT NULL
Recette(#id=>Post(id), tempsPrep: time, tempsCuisson: time, nbPersonnes: int)

Question(#id:int, intitule:string, contenu:text)
	avec intitule NOT NULL

BonCommande(#id:int, titre:string, envoye:bool, passe:bool, utilisateur=>Utilisateur(login))
    avec utilisateur NOT NULL et pas passé sans être envoyé
Article(#id:int, #bon=>BonCommande(id), titre:string, prix:float, qte:int)

